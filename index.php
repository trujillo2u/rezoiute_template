<?php
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();

//javascript files
$doc->addScript('templates/' . $this->template . '/javascripts/jquery/jquery.js');

//obtenir fichiers css
$doc->addStyleSheet('templates/' . $this->template . '/css/styles.css');


$this->language = $doc->language;
$this->direction = $doc->direction;


// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

// Logo file or site title param
if ($this->params->get('logoFile')) {
    $logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
} else if ($this->params->get('sitetitle')) {
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle')) . '</span>';
} else {
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <jdoc:include type="head" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <?php // Template color ?>

            <style type="text/css">
<?php if ($this->params->get('templateColor')) : ?>
                    body.site
                    {
                        background-color: <?php echo $this->params->get('templateBackgroundColor'); ?>;
                    }
                    .header-search ul li[class*=current]:hover{
                        background-color: <?php echo $this->params->get('templateBackgroundColor'); ?>;
                        border-bottom: 3px solid <?php echo $this->params->get('templateBackgroundColor'); ?>;
                    }
                                                                                                    
<?php endif; ?>
<?php if ($this->params->get('fontColor')) : ?>
                    .header-search ul li[class*=current]{
                        border-bottom: 3px solid <?php echo $this->params->get('fontColor'); ?>;
                    }
                    .header-search ul li a{
                        color: <?php echo $this->params->get('fontColor'); ?> ;
                    }
                    .header-search ul li a:hover{
                        background-color: <?php echo $this->params->get('fontColor'); ?>;
                    }
<?php endif; ?>
                .menu{
<?php if ($this->params->get('mainMenuFontSize')) : ?>
                        font-size: <?php echo $this->params->get('mainMenuFontSize'); ?> ;
<?php endif; ?>
                }
                
                .btn-primary{
<?php if ($this->params->get('buttonBackgroundColor')) : ?>
                        background-color: <?php echo $this->params->get('buttonBackgroundColor'); ?>;
<?php endif; ?>
<?php if ($this->params->get('buttonColor')) : ?>
                        color: <?php echo $this->params->get('buttonColor'); ?>;
<?php endif; ?>
                }
                .btn-primary:hover{
<?php if ($this->params->get('buttonBackgroundColor')) : ?>
                        color: <?php echo $this->params->get('buttonBackgroundColor'); ?>;
<?php endif; ?>
<?php if ($this->params->get('buttonColor')) : ?>
                        background-color: <?php echo $this->params->get('buttonColor'); ?>;
<?php endif; ?>
<?php if ($this->params->get('buttonBackgroundColor')) : ?>
                        border: solid 1px <?php echo $this->params->get('buttonBackgroundColor'); ?>;
                        -webkit-box-shadow: 1px 0px 6px 0px <?php echo $this->params->get('buttonBackgroundColor'); ?>;
                        -moz-box-shadow:    1px 0px 6px 0px <?php echo $this->params->get('buttonBackgroundColor'); ?>;
                        box-shadow:         1px 0px 6px 0px <?php echo $this->params->get('buttonBackgroundColor'); ?>;
<?php endif; ?>
                }

                input[type=text], input[type=password], input[type=tel], input[type=url],input[type=search], input[type=email],textarea,.btn-primary		{

<?php if ($this->params->get('inputsBorderColor')) : ?>
                        border: solid 1px <?php echo $this->params->get('inputsBorderColor'); ?>;
                        -webkit-box-shadow: 1px 0px 6px 0px <?php echo $this->params->get('inputsColor'); ?>;
                        -moz-box-shadow:    1px 0px 6px 0px <?php echo $this->params->get('inputsColor'); ?>;
                        box-shadow:         1px 0px 6px 0px <?php echo $this->params->get('inputsColor'); ?>;
<?php endif; ?>
                }
                input[type=text], input[type=password], input[type=tel], input[type=url],input[type=search], input[type=email],textarea		{
<?php if ($this->params->get('inputsColor')) : ?>
                        background: <?php echo $this->params->get('inputsColor'); ?>;
<?php endif; ?>
                }

                .content{
<?php if ($this->params->get('contentFontSize')) : ?>
                        font-size: <?php echo $this->params->get('contentFontSize'); ?>;
<?php endif; ?>
<?php if ($this->params->get('contentFontColor')) : ?>
                        color: <?php echo $this->params->get('contentFontColor'); ?>;
<?php endif; ?> 

                }
                a{
<?php if ($this->params->get('lienstFontSize')) : ?>
                        font-size: <?php echo $this->params->get('liensFontSize'); ?>;
<?php endif; ?>
<?php if ($this->params->get('liensFontColor')) : ?>
                        color: <?php echo $this->params->get('liensFontColor'); ?>;
<?php endif; ?> 
                }
                a:focus{
                    
                }
                .hr-menu{
<?php if ($this->params->get('hrSize')) : ?>
                        height:  <?php echo $this->params->get('hrSize'); ?>;
<?php endif; ?>
<?php if ($this->params->get('hrColor')) : ?>
                        background: <?php echo $this->params->get('hrColor'); ?>; 
                        background-image: -webkit-linear-gradient(left, #fff, <?php echo $this->params->get('hrColor'); ?>, #fff); 
                        background-image: -moz-linear-gradient(left, #fff, <?php echo $this->params->get('hrColor'); ?>, #fff); 
                        background-image: -ms-linear-gradient(left, #fff, <?php echo $this->params->get('hrColor'); ?>, #fff); 
                        background-image: -o-linear-gradient(left, #fff, <?php echo $this->params->get('hrColor'); ?>, #fff); F
<?php endif; ?>
                }
                .aside > div{
<?php if ($this->params->get('hrColor')) : ?>
                        border: solid 1px <?php echo $this->params->get('hrColor'); ?>;
<?php endif; ?>
                }
                .last-articles ul li, .options-user ul li{
<?php if ($this->params->get('hrColor')) : ?>
                        border-bottom: 1px solid<?php echo $this->params->get('hrColor'); ?>;
<?php endif; ?>
                }
            </style>
            <!--[if lt IE 9]>
                    <script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
            <![endif]-->
    </head>
    <body class="site">

        <!-- Body -->
        <div class="body">

            <!-- main container -->
            <div class='container'>


                <!-- Header -->
                <div class="header-menu clearfix">
                    <header class="header clearfix" role="banner">
                        <div class="logo">
                            <a class="pull-left" href="<?php echo $this->baseurl; ?>">
                                <?php echo $logo; ?>
                                <?php if ($this->params->get('sitedescription')) : ?>
                                    <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
                                <?php endif; ?>
                            </a>
                        </div>
                        <a class="show-menu-label">Menu</a>
                        <div class="header-search pull-left col-md-5" id="menu">
                            <jdoc:include type="modules" name="menu" style="well" />
                        </div>
                        <!-- right sidebar -->
                        <a class="show-login-label">Login</a>
                        <div class=" text-center col-md-6 pull-right" id="form-login-header">
                            <div  class='form-login-header clearfix'>
                                <jdoc:include type="modules" name="formulaire-login" style="well" />
                            </div>
                        </div>
                    </header>
                </div>
                <hr class="hr-menu">
                    <div class="row">
                        <jdoc:include type="modules" name="slide" style="xhtml" />
                    </div>
                    <div class="row">
                        <div class="col-md-6" id="">
                            <jdoc:include type="modules" name="breadcrums" style="xhtml" />
                        </div>
                        <div class="col-md-6" id="search-bar">
                            <jdoc:include type="modules" name="recherche" style="xhtml" />
                        </div>
                    </div>

                    <div class='row content'> 

                        <!-- main content area -->
                        <div class='col-lg-9 col-md-9 col-xs-12'>
                            <jdoc:include type="message" style="xhtml" />
                            <jdoc:include type="component" style="xhtml" />
                            <jdoc:include type="modules" name="extra" style="xhtml" />
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 aside">
                            <div class="col-lg-12 col-xs-12 col-sm-6 col-md-12 options-user">
                                <jdoc:include type="modules" name="options-utilisateur" style="xhtml" />
                            </div>
                            <div class="col-lg-12 col-xs-12 col-sm-6 col-md-12 last-articles">
                                <jdoc:include type="modules" name="derniers-articles" style="xhtml" />
                            </div>
                        </div>

                    </div>
                    <!-- footer -->
                    <div class='row footer'>
                        <div class='span12'>Footer
                            <jdoc:include type="modules" name="footer" style="xhtml" />
                        </div>
                    </div>
            </div>
        </div>
        <?php
//javascript files
        $doc->addScript('templates/' . $this->template . '/javascripts/menu.js');
        $doc->addScript('templates/' . $this->template . '/javascripts/recherche-avance.js');
        ?>
    </body>
</html>
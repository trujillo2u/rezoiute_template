<?php
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();


$this->language = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

$sitename = $app->get('sitename');
//javascript files
$doc->addScript('/templates/' . $this->template . '/javascripts/jquery/jquery.js');

//obtenir fichiers css
$doc->addStyleSheet('/templates/' . $this->template . '/css/styles.css');


// Logo file or site title param
if ($params->get('logoFile')) {
    $logo = '<img src="' . JUri::root() . $params->get('logoFile') . '" alt="' . $sitename . '" />';
} elseif ($params->get('sitetitle')) {
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($params->get('sitetitle')) . '</span>';
} else {
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo $this->title; ?> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="<?php echo  $this->baseurl.'templates/' . $this->template . '/css/styes.css'; ?>" />

        <!--[if lt IE 9]>
                <script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
        <![endif]-->
    </head>

    <body class="site">

        <!-- Body -->
        <div class="body">

            <!-- main container -->
            <div class='container'>


                <!-- Header -->
                <div class="header-menu clearfix">
                    <header class="header clearfix" role="banner">
                        <div class="logo">
                            <a class="pull-left" href="<?php echo $this->baseurl; ?>">
                                <?php echo $logo; ?>
                            </a>
                        </div>
                        <a class="show-menu-label">Menu</a>
                        <div class="header-search pull-left col-md-5" id="menu">
                            <jdoc:include type="modules" name="menu" style="well" />
                        </div>
                        <!-- right sidebar -->
                        <a class="show-login-label">Login</a>
                        <div class=" text-center col-md-6 pull-right" id="form-login-header">
                            <div  class='form-login-header clearfix'>
                                <jdoc:include type="modules" name="formulaire-login" style="well" />
                            </div>
                        </div>
                    </header>
                </div>
                <hr class="hr-menu">
                    <div class="row">
                        <jdoc:include type="modules" name="slide" style="xhtml" />
                    </div>
                    <div class="row">
                        <div class="col-md-6" id="">
                            <jdoc:include type="modules" name="breadcrums" style="xhtml" />
                        </div>
                        <div class="col-md-6" id="search-bar">
                            <jdoc:include type="modules" name="recherche" style="xhtml" />
                        </div>
                    </div>

                    <div class='row content'> 

                       <!-- Begin Content -->
					<h1 class="page-header"><?php echo JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'); ?></h1>
					<div class="well">
						<div class="row-fluid">
							<div class="span6">
								<p><strong><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></strong></p>
								<p><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></p>
								<ul>
									<li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
								</ul>
							</div>
							<div class="span6">
								<?php if (JModuleHelper::getModule('search')) : ?>
									<p><strong><?php echo JText::_('JERROR_LAYOUT_SEARCH'); ?></strong></p>
									<p><?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?></p>
									<?php echo $doc->getBuffer('module', 'search'); ?>
								<?php endif; ?>
								<p><?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?></p>
								<p><a href="<?php echo $this->baseurl; ?>/index.php" class="btn"><i class="icon-home"></i> <?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></p>
							</div>
						</div>
						<hr />
						<p><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></p>
						<blockquote>
							<span class="label label-inverse"><?php echo $this->error->getCode(); ?></span> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8');?>
						</blockquote>
					</div>
					<!-- End Content -->

                    </div>
                    <!-- footer -->
                    <div class='row footer'>
                        <div class='span12'>Footer
                            <jdoc:include type="modules" name="footer" style="xhtml" />
                        </div>
                    </div>
            </div>
        </div>
        <?php
//javascript files
        $doc->addScript('templates/' . $this->template . '/javascripts/menu.js');
        $doc->addScript('templates/' . $this->template . '/javascripts/recherche-avance.js');
        ?>
    </body>
</html>
